#include "postgres.h"

// #include "catalog/pg_collation.h"
// #include "common/hashfn.h"
#include "utils/builtins.h"
#include "utils/formatting.h"
#include "utils/varlena.h"

PG_MODULE_MAGIC;


PG_FUNCTION_INFO_V1(oranullin);

Datum
oranullin(PG_FUNCTION_ARGS)
{
    char    *inputText;
    char    *emptyStr = "";

    inputText = text_to_cstring(PG_GETARG_TEXT_P(0));

    if (PG_ARGISNULL(0))
        PG_RETURN_NULL();

    if (strcmp(inputText, emptyStr) == 0)
    {
        PG_RETURN_NULL(); 
    } else 
    {
        PG_RETURN_TEXT_P(PG_GETARG_TEXT_P(0));
    }

}