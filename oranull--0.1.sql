/* oranull 0.1 version */

-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION oranull" to load this file. \quit

--
--  Input and output functions.
--
CREATE FUNCTION oranullin(text)
RETURNS oranull
AS 'MODULE_PATHNAME','oranullin'
LANGUAGE C IMMUTABLE  PARALLEL SAFE;

CREATE FUNCTION oranullout(oranull)
RETURNS text
AS 'textout'
LANGUAGE internal IMMUTABLE PARALLEL SAFE;

--require review to custom function to analyze ''
CREATE FUNCTION oranullrecv(internal)
RETURNS oranull
AS 'textrecv'
LANGUAGE internal STABLE PARALLEL SAFE;

CREATE FUNCTION oranullsend(oranull)
RETURNS bytea
AS 'textsend'
LANGUAGE internal STABLE PARALLEL SAFE;

--
--  The type itself.
--

CREATE TYPE oranull (
    INPUT          = oranullin,
    OUTPUT         = oranullout,
    RECEIVE        = oranullrecv,
    SEND           = oranullsend,
    LIKE           = TEXT,
    INTERNALLENGTH = VARIABLE,
    STORAGE        = extended,
    -- make it a non-preferred member of string type category
    CATEGORY       = 'S',
    PREFERRED      = false,
    COLLATABLE     = true
);


